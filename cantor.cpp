#include <iostream>
#include <cstdlib>
#include <ctime>
#include <map>

using namespace std;

const int n = 10000;

bool prob_true(double p){
    return rand()/(RAND_MAX+1.0) < p;
}

bool melysegi(bool a[][n],int x,int y, bool explored[][n]){
    explored[y][x]=true;

    if(a[y][x]){
        if(y==n-1) return true;
        else if(!explored[y+1][x] && melysegi(a,x,y+1,explored)) return true;
        else if(x<n-1) if(!explored[y][x+1] && melysegi(a,x+1,y,explored)) return true;
        else if(x>0) if(!explored[y][x-1] && melysegi(a,x-1,y,explored)) return true;
        else if(y>0) if(!explored[y-1][x] && melysegi(a,x,y-1,explored)) return true;
        return false;
    }else return false;
}

bool is_good(bool s[][n]){

    bool explored[n][n];

    for(int i = 0; i<n; i++){

        for(int a=0; a<n; a++){
            explored[i][a] = false;

        }

    }

    for(int i = 0; i<n;i++){
        if(melysegi(s,i,0,explored)) return true;
    }

    return false;
}

pair<double, int> valszam(double p, int testnum)
{
    srand(time(0));
    int counter=0;
    int c=0;
    
    testnum *= 8;

//    cout<<"start\n";
    float timeStart = clock();
    bool a[n][n];
  
    while(true){
#pragma omp parallel for
        for(int i = 0; i<n; i++){
            
            for(int c = 0; c<n; c++){
                a[i][c] = prob_true(p);
            }
           
        }
        bool jee=is_good(a);
        if(jee) counter++;
        c++;
       
        if ((clock() - timeStart) / CLOCKS_PER_SEC >= testnum*60) // time in seconds
        break;
    }

//    cout << (float) counter/c<<endl<<c;
return {(double)counter/c,c};
}

int main() {
    double P=0;
    int db;
   
    int eps;
    cout<<"What is epsilon: ";
    cin>>eps;
    int runtime=30;
    
    double tops=0.7;
    double bots=0.5;
    
    double p;
    
    while(true){
        p=(tops+bots)/2;
      
	pair<double, int> res = valszam(p,runtime);
	P = res.first;
	db = res.second;
       
        if(db>0){
            if(P<0.5) bots=p;
            else if(P>0.5) tops=p;
            
            if(tops-bots<eps) break;
        }else{
            runtime*=2;
        }
        cout<<"Tops: "<<tops<<", Bots: "<<bots<<" -> " << P << ", " << db <<endl;
    }
    cout<<"Ending:"<<tops<<","<<bots<<".\n";
    return 0;
}